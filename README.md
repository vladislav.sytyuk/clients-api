Clients API


### Description: 
Clients API allows users to filter client's data and retrieve it in json or upload it to csv.


## Installation:

Clone the project and then go to the root directory and create .env file like in .env.example 
(You might want to change the credentials). Then you can install app:


### Installation with docker:

Go to the root dir, build and start containers:

```commandline
docker-compose up --build
```
Then apply migrations:
```commandline
docker-compose exec backend alembic upgrade head
```
Run script which loads data from csv to database:
```commandline
docker-compose exec backend python ./app/csv_upload_script.py
```

## Usage:


Swagger documentation is available at http://127.0.0.1:8000/docs#/
