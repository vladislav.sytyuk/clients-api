from typing import Optional

from fastapi_filter.contrib.sqlalchemy import Filter

from app.models import Client


class ClientFilter(Filter):
    category: Optional[str] = None
    gender: Optional[str] = None
    birthDate: Optional[str] = None
    age: Optional[int] = None
    age__gte: Optional[int] = None
    age__lte: Optional[int] = None

    class Constants(Filter.Constants):
        model = Client
