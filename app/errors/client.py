from starlette import status

from app.errors.base import BaseError


class ClientsNotFoundError(BaseError):
    def __init__(self):
        status_code = status.HTTP_404_NOT_FOUND
        detail = "No clients found"
        super(ClientsNotFoundError, self).__init__(status_code=status_code, detail=detail)
