from sqlalchemy import Column, String, Date, Integer

from app.db.session import Base


class Client(Base):
    __tablename__ = "clients"

    id = Column(Integer, primary_key=True)
    category = Column(String)
    firstname = Column(String)
    lastname = Column(String)
    email = Column(String)
    gender = Column(String)
    birthDate = Column(Date, name="birthDate")
    age = Column(Integer)
