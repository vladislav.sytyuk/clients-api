from fastapi import APIRouter, Depends
from fastapi.responses import StreamingResponse
from fastapi_filter import FilterDepends
from fastapi_pagination import Page, paginate

from sqlalchemy.orm import Session

from app.services.client import ClientService
from app.schemas.client import Client
from app.db.session import get_db
from app.filters.client import ClientFilter


router = APIRouter()


@router.get("/")
def get_clients(
    client_filter: ClientFilter = FilterDepends(ClientFilter),
    db: Session = Depends(get_db),
) -> Page[Client]:
    service = ClientService(db)
    clients = service.get_clients(client_filter)
    return paginate(clients)


@router.get("/csv/", response_class=StreamingResponse)
async def export_csv(
    client_filter: ClientFilter = FilterDepends(ClientFilter),
    db: Session = Depends(get_db),
):
    service = ClientService(db)
    data_stream = service.get_clients_csv(client_filter)
    response = StreamingResponse(iter([data_stream.getvalue()]), media_type="text/csv")
    response.headers["Content-Disposition"] = "attachment; filename=clients.csv"
    return response
