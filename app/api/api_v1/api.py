from fastapi import APIRouter

from app.api.api_v1.endpoints import client


api_router = APIRouter(prefix="/api/v1")

api_router.include_router(client.router, prefix="/clients", tags=["Clients"])
