import csv
from datetime import datetime

from sqlalchemy.exc import IntegrityError

from app.models import Client
from app.db.session import SessionLocal


session = SessionLocal()


def calculate_age(birthdate: str) -> int:
    birthdate = datetime.strptime(birthdate, "%Y-%m-%d").date()
    current_date = datetime.now().date()
    age = (
        current_date.year
        - birthdate.year
        - ((current_date.month, current_date.day) < (birthdate.month, birthdate.day))
    )
    return age


with open("dataset.csv", "r") as csvfile:
    csvreader = csv.DictReader(csvfile)
    rows_to_insert = []
    for row in csvreader:
        row["age"] = calculate_age(row["birthDate"])
        rows_to_insert.append(row)
    try:
        session.bulk_insert_mappings(Client, rows_to_insert)
        session.commit()
        print("Data uploaded successfully to the database.")
    except IntegrityError as e:
        session.rollback()
        print("Error:", e)

session.close()
