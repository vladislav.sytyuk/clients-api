import io

from sqlalchemy import select

import pandas as pd

from app.models import Client
from app.services.base import BaseService
from app.filters.client import ClientFilter
from app.errors.client import ClientsNotFoundError


class ClientService(BaseService):

    def get_clients(self, client_filter: ClientFilter):
        query = client_filter.filter(select(Client))
        clients = self.db.execute(query).scalars().all()
        if not clients:
            raise ClientsNotFoundError()
        return clients

    def get_clients_csv(self, client_filter: ClientFilter):
        clients_query = self.db.query(Client).with_entities(
            Client.category,
            Client.firstname,
            Client.lastname,
            Client.email,
            Client.gender,
            Client.birthDate,
        )
        data_frame = pd.read_sql(client_filter.filter(clients_query).statement, self.db.bind)
        if data_frame.empty:
            raise ClientsNotFoundError()
        data_stream = io.StringIO()
        data_frame.to_csv(data_stream, index=False)
        return data_stream
